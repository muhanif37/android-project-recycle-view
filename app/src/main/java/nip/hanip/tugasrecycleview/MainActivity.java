package nip.hanip.tugasrecycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

        RecyclerView recyclerView;
        MahasiswaAdapter adapter;
        ArrayList<Mahasiswa> mahasiswaList;
        EditText etNama, etNim;
        Button button;
        @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            button = findViewById(R.id.button);
            button.setOnClickListener(this);
            loadData();
            initRecycleView();


        }

        private void initRecycleView(){
            RecyclerView recyclerView = findViewById(R.id.recycleView);
            adapter = new MahasiswaAdapter(mahasiswaList,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }

        private void addData(){
            etNama = findViewById(R.id.etNama);
            etNim = findViewById(R.id.etNim);
            mahasiswaList.add(new Mahasiswa(etNama.getText().toString(),etNim.getText().toString()));
        }

        private void loadData(){
            mahasiswaList = new ArrayList<>();
            mahasiswaList.add(new Mahasiswa("Hanif","235150709111001"));
            mahasiswaList.add(new Mahasiswa("Jendral","002"));
            mahasiswaList.add(new Mahasiswa("Adi","003"));
        }

    @Override
    public void onClick(View view) {
        if (view.getId()==button.getId())
        {
            addData();
            adapter.notifyDataSetChanged();
        }
    }
}
