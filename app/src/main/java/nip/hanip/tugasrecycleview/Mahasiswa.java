package nip.hanip.tugasrecycleview;

import android.widget.Button;
import android.widget.EditText;

public class Mahasiswa {

    private String nama,nim;

    public Mahasiswa(String nama, String nim) {
        this.nama = nama;
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public String getNim() {
        return nim;
    }
}
