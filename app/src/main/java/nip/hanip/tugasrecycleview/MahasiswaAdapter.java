package nip.hanip.tugasrecycleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaViewHolder> {

    private ArrayList<Mahasiswa> mahasiswaList;
    private Context context;

    public MahasiswaAdapter(ArrayList<Mahasiswa> mahasiswaList, Context context) {
        this.mahasiswaList = mahasiswaList;
        this.context = context;
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,parent,false);
        MahasiswaViewHolder viewHolder = new MahasiswaViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {
        holder.getTvDisplayNama().setText(mahasiswaList.get(position).getNama());
        holder.getTvDisplayNim().setText(mahasiswaList.get(position).getNim());
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }
}
