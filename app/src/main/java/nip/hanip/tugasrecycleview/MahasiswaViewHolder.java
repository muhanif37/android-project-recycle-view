package nip.hanip.tugasrecycleview;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder {
    TextView tvDisplayNama,tvDisplayNim;
    Context context;

    public MahasiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        context = itemView.getContext();
        tvDisplayNama = itemView.findViewById(R.id.tvDisplayNama);
        tvDisplayNim = itemView.findViewById(R.id.tvDisplayNim);
    }

    public TextView getTvDisplayNama() {
        return tvDisplayNama;
    }

    public TextView getTvDisplayNim() {
        return tvDisplayNim;
    }

    public Context getContext() {
        return context;
    }
}
